package ru.t1.kharitonova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.util.TerminalUtil;

public final class TaskStartByIndexCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Start task by index.";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-start-by-index";
    }

    @Override
    public void execute() {
        System.out.println("[START TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final String userId = getUserId();
        getTaskService().changeTaskStatusByIndex(userId, index, Status.IN_PROGRESS);
    }

}
